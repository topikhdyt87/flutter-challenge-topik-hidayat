import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_challenge/app/routes/app_pages.dart';
import 'package:flutter_challenge/app/services/auth_api.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LoginController extends GetxController {
  TextEditingController userId = TextEditingController(text: 'kminchelle');
  TextEditingController password = TextEditingController(text: '0lelplR');

  GetStorage box = GetStorage();

  // Utils
  RxBool isLoading = false.obs;

  Future<void> login() async {
    if (userId.text.trim().isEmpty || password.text.trim().isEmpty) {
      showPopup(
        title: 'Perhatian!',
        body: 'User ID dan atau Password anda belum diisi.',
        onTap: () => Get.back(),
      );
    } else {
      isLoading.value = true;
      Map response = await AuthApi().login(userId.text, password.text);
      isLoading.value = false;

      if (response['status']) {
        box.write('token', response['data']['token']);
        box.write('user', jsonEncode(response['data']));
        showPopup(
          title: 'Berhasil!',
          body: 'Login Berhasil',
          onTap: () => Get.offAllNamed(Routes.HOME),
        );
      } else {
        showPopup(
          title: 'Perhatian!',
          body: 'User ID dan atau Password anda salah silahkan coba lagi',
          onTap: () => Get.back(),
        );
      }
    }

    isLoading.value = false;
  }

  void showPopup({
    required String title,
    required String body,
    required VoidCallback onTap,
  }) {
    showDialog(
      barrierDismissible: false,
      context: Get.context!,
      builder: (BuildContext context) {
        return AlertDialog(
          actionsPadding: const EdgeInsets.fromLTRB(15, 10, 15, 20),
          title: Text(title),
          content: Text(body),
          actionsAlignment: MainAxisAlignment.center,
          actions: [
            SizedBox(
              width: double.infinity,
              height: 40,
              child: TextButton(
                style: TextButton.styleFrom(
                  backgroundColor: const Color(0xFF7F01B3),
                ),
                onPressed: onTap,
                child: const Text(
                  "OK",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            )
          ],
        );
      },
    );
  }
}
