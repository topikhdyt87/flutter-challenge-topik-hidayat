import 'dart:convert';

import 'package:flutter_challenge/app/routes/app_pages.dart';
import 'package:flutter_challenge/app/services/home_api.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class HomeController extends GetxController {
  final GetStorage box = GetStorage();

  RxMap user = {}.obs;
  RxBool isLoading = true.obs;
  RxList products = [].obs;

  @override
  void onInit() {
    user.value = jsonDecode(box.read('user'));
    getProducts();
    super.onInit();
  }

  Future<void> getProducts() async {
    Map response = await HomeApi().get();
    if (response['status']) {
      products.value = response['data']['products'];
    }
    isLoading.value = false;
  }

  void logout() {
    box.erase();
    Get.offAllNamed(Routes.LOGIN);
  }
}
