import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/splash_screen_controller.dart';

class SplashScreenView extends GetView<SplashScreenController> {
  const SplashScreenView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Image.asset("assets/header-splash.png"),
          Align(
            alignment: Alignment.center,
            child: Image.asset("assets/logo.png"),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: Image.asset("assets/footer-splash.png"),
          )
        ],
      ),
    );
  }
}
