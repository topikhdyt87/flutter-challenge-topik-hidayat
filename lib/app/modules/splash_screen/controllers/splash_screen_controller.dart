import 'package:flutter_challenge/app/routes/app_pages.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class SplashScreenController extends GetxController {
  GetStorage box = GetStorage();

  @override
  void onInit() {
    Future.delayed(const Duration(seconds: 2), () => autoLogin());
    super.onInit();
  }

  autoLogin() {
    if (box.hasData('token')) {
      Get.offNamed(Routes.HOME);
    } else {
      Get.offNamed(Routes.LOGIN);
    }
  }
}
