import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter_challenge/app/configs/shared_api.dart';

class AuthApi extends SharedApi {
  Future<Map> login(String username, String password) async {
    try {
      final response = await http.post(
        Uri.parse('$baseURI/auth/login'),
        headers: {'Content-type': 'application/json'},
        body: jsonEncode({
          "username": username,
          "password": password,
        }),
      );

      var data = json.decode(response.body);
      if (response.statusCode == 200) {
        return {
          'status': true,
          'data': data,
        };
      } else {
        return {
          'status': false,
        };
      }
    } catch (exception) {
      return {
        'status': false,
        'message': 'Something went wrong, Please try again later',
      };
    }
  }
}
