import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter_challenge/app/configs/shared_api.dart';

class HomeApi extends SharedApi {
  Future<Map> get() async {
    try {
      final response = await http.get(Uri.parse('$baseURI/products'));

      var data = json.decode(response.body);

      if (response.statusCode == 200) {
        return {
          'status': true,
          'data': data,
        };
      } else {
        return {
          'status': false,
        };
      }
    } catch (exception) {
      return {
        'status': false,
      };
    }
  }
}
