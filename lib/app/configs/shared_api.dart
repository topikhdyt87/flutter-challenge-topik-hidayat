import 'package:get_storage/get_storage.dart';

class SharedApi {
  final String baseURI = 'https://dummyjson.com';
  final GetStorage box = GetStorage();

  Map<String, String> getToken() {
    if (box.hasData("token") == true) {
      return {
        'Authorization': "Bearer ${box.read('token')}",
        'Content-type': 'Application/Json',
      };
    } else {
      return {
        'Authorization': 'Bearer Bad Token',
        'Content-type': 'Application/Json',
      };
    }
  }
}
